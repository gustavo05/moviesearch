package com.mobileplus.moviesearch.model.network

import com.mobileplus.moviesearch.model.entities.SearchResponse
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface MovieApi {

    @GET
    fun search(
        @Url url: String = "",
        @Query("s") query: String,
        @Query("page") page: Int? = 1
    ): Flow<SearchResponse>
}
