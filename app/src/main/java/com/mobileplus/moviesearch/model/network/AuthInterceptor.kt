package com.mobileplus.moviesearch.model.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .url(
                    request()
                        .url
                        .newBuilder()
                        .addQueryParameter("apiKey", "4eb49783")
                        .build()
                )
                .build()
        )

    }
}