package com.mobileplus.moviesearch.model.entities

import com.squareup.moshi.Json

data class Movie(
    @Json(name = "Title") val title: String,
    @Json(name = "Year") val year: String,
    @Json(name = "imdbID") val movieId: String,
    @Json(name = "Type") val type: String,
    @Json(name = "Poster") val poster: String
)
