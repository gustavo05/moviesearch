package com.mobileplus.moviesearch.model.entities

import com.squareup.moshi.Json

data class SearchResponse(
    @Json(name = "Search") val search: List<Movie>?,
    @Json(name = "totalResults") val totalResults: Int?
)
